package Temperatures;

import java.util.*;

/**
 * Auto-generated code below aims at helping you parse the standard input
 * according to the problem statement.
 **/
class Solution {

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez le nombre de temp�rature : ");
		int n = in.nextInt(); // the number of temperatures to analyse
		System.out.println(n);

		int res = 0;
		int tab[] = new int[n];
		System.out.println("Entrez vos " + n + " temp�rature(s), une � la fois : ");
		for (int i = 0; i < n; i++) {
			int t = in.nextInt(); // a temperature expressed as an integer ranging from -273 to 5526
			tab[i] = t; // Ajout des temp�ratures dans un tableau
		}

		int temp = 0;
		int minPositif = Integer.MAX_VALUE;
		int maxNegatif = Integer.MIN_VALUE;
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] == 0) {
				temp = 0;
			} else if (tab[i] > 0) { // On r�cup�re la valeur positive la plus proche de z�ro
				for (int j = 0; j < tab.length; j++) {
					temp = tab[i];
					if (minPositif > temp) {
						minPositif = temp;
					}
				}
			} else { // On r�cup�re la valeur n�gative la plus proche de z�ro
				for (int j = 0; j < tab.length; j++) {
					temp = tab[i];
					if (maxNegatif < temp) {
						maxNegatif = temp;
					}
				}
			}
			if (minPositif > Math.abs(maxNegatif)) {
				res = maxNegatif;
			} else if (tab[i] == 0) {
				res = 0;
			} else {
				res = minPositif;
			}
		}

		// Write an action using System.out.println()
		// To debug: System.err.println("Debug messages...");

		System.out.println("result" + res);
	}
}
